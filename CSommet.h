#pragma once
#ifndef CSOMMET
#define CSOMMET

#include "CException.h"
#include "CArc.h"

#define SOMIndice_Incorrect 20
#define SOMAjouter_Arrivant_Impossible 21
#define SOMAjouter_Partant_Impossible 22
#define SOMSupprimer_Arrivant_Impossible 23
#define SOMSupprimer_Partant_Impossible 24


class CSommet
{
private:
	int eSOMnumero;
	CArc** pSOMarcs_Partant; // Tableau � 2 dimensions qui stocke les arcs partant de ce sommet
	CArc** pSOMarcs_Arrivant; // Tableau � 2 dimensions qui stocke les arcs arrivant sur ce sommet
	unsigned int nNbArcsPartant;
	unsigned int nNbArcsArrivant;

public:

	/**********************************************************************************************
	Constructeur par d�faut permettant d�initialiser un sommet
	***********************************************************************************************
	Entr�e : le num�ro voulu pour le sommet
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CSommet(int eNumero);


	/***********************************************************************************************
	M�thode SOMLire_Numero permettant de renvoyer le num�ro du sommet
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (Le num�ro du sommet est renvoy�)
	**********************************************************************************************/
	inline int SOMLire_Numero();


	/***********************************************************************************************
	M�thode SOMModifier_Numero permettant de modifier le num�ro du sommet
	***********************************************************************************************
	Entr�e : le num�ro voulu pour le sommet
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	inline void SOMModifier_Numero(int eNumero);

	/***********************************************************************************************
	M�thode SOMLire_Taille_Partant permettant de renvoyer la taille utilis�e du tableau d'arcs partants
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (La taille utilis�e du tableau d'arcs partants est renvoy�e)
	**********************************************************************************************/
	unsigned int SOMLire_Taille_Partant();

	/***********************************************************************************************
	M�thode SOMLire_Taille_Arrivant permettant de renvoyer la taille utilis�e du tableau d'arcs arrivants
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (La taille utilis�e du tableau d'arcs arrivants est renvoy�e)
	**********************************************************************************************/
	unsigned int SOMLire_Taille_Arrivant();


	/***********************************************************************************************
	M�thode SOMAjouter_Arrivant permettant d'ajouter un arc arrivant sur ce sommet
	***********************************************************************************************
	Entr�e : un pointeur sur l'arc � ajouter
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception SOMAjouter_Arrivant_Impossible : Ajout d'un sommet arrivant impossible)
	**********************************************************************************************/
	void SOMAjouter_Arrivant(CArc* pARCParam);

	/***********************************************************************************************
	M�thode SOMAjouter_Partant permettant d'ajouter un arc partant de ce sommet
	***********************************************************************************************
	Entr�e : un pointeur sur l'arc � ajouter
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception SOMAjouter_Partant_Impossible : Ajout d'un sommet partant impossible)
	**********************************************************************************************/
	void SOMAjouter_Partant(CArc* pARCParam);
	
	/***********************************************************************************************
	M�thode SOMSupprimer_Arrivant permettant de supprimer un arc arrivant sur ce sommet
	***********************************************************************************************
	Entr�e : un pointeur sur l'arc � supprimer
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception SOMSupprimer_Arrivant_Impossible : Suppression d'un sommet arrivant 
														     impossible )
	**********************************************************************************************/
	void SOMSupprimer_Arrivant(CArc* pARCParam);

	/***********************************************************************************************
	M�thode SOMSupprimer_Partant permettant de supprimer un arc partant de ce sommet
	***********************************************************************************************
	Entr�e : un pointeur sur l'arc � supprimer
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception SOMSupprimer_Partant_Impossible : Suppression d'un sommet partant 
														 impossible )
	**********************************************************************************************/
	void SOMSupprimer_Partant(CArc* pARCParam);

	/***********************************************************************************************
	M�thode SOMLire_Partant permettant de renvoyer le contenu du tableau d'arcs partants
	***********************************************************************************************
	Entr�e : l'indice dans le tableau de l'arc � renvoyer
	N�cessite : n�ant
	Sortie : Un pointeur sur l'arc pSOMarcs_Partant[nIndice]
	Entra�ne : (L�objet en cours est modifi�) ou
			   (Exception SOMIndice_Incorrect : indice donn�e incorrect)
	**********************************************************************************************/
	CArc* SOMLire_Partant(unsigned int nIndice);

	/***********************************************************************************************
	M�thode SOMLire_Arrivant permettant de renvoyer le contenu du tableau d'arcs arrivants
	***********************************************************************************************
	Entr�e : l'indice dans le tableau de l'arc � renvoyer
	N�cessite : n�ant
	Sortie : Un pointeur sur l'arc pSOMarcs_Arrivant[nIndice]
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception SOMIndice_Incorrect : indice donn�e incorrect)
	**********************************************************************************************/
	CArc* SOMLire_Arrivant(unsigned int nIndice);

};

#include "CSommet.cpp"

#endif