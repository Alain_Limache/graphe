/**********************************************************************************************
Constructeur par d�faut permettant d�initialiser un graphe
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
CGraphe::CGraphe()
{
	nNbSommets = 0;
	pGRAtab_Sommets = (CSommet**)malloc(sizeof(CSommet));
}


/**********************************************************************************************
M�thode GRAAfficher permettant d'afficher le graphe
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : Affichage console
Entra�ne : (Affichage console du graphe)
**********************************************************************************************/
void CGraphe::GRAAfficher()
{
	std::cout << "Liste des sommets : ";
	for (unsigned int nBoucle = 0; nBoucle < GRALire_Taille(); nBoucle++)
	{
		std::cout << "S" << GRALire_Tab(nBoucle)->SOMLire_Numero() ;
		if (nBoucle+1 < GRALire_Taille()) {
			std::cout << " , ";
		}
	}

	std::cout << "\nListe des arcs : \n";

	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Partant(); nBoucle_j++)
		{
			std::cout << "S" << GRALire_Tab(nBoucle_i)->SOMLire_Numero() << " -> S" << GRALire_Tab(nBoucle_i)->SOMLire_Partant(nBoucle_j)->ARCLire_Numero_Destination() << "\n";
		}
	}
}


/**********************************************************************************************
M�thode GRALire_Taille permettant de renvoyer le nombre de sommets du graphe
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (La taille du tableau / le nombre de sommets est renvoy�(e))
**********************************************************************************************/
unsigned int CGraphe::GRALire_Taille()
{
	return nNbSommets;
}


/**********************************************************************************************
Methode GRALire_Tab permettant de lire un sommet du graphe
***********************************************************************************************
Entr�e : l'indice du sommet dans le tableau du graphe
N�cessite : n�ant
Sortie : un pointeur sur un sommet du tableau
Entra�ne : (Retourne un �l�ment de type CSommet*) ou
		   (Exception GRAIndice_Incorrect : indice donn�e incorrect)
**********************************************************************************************/
inline CSommet* CGraphe::GRALire_Tab(unsigned int nIndice)
{
	if (nIndice < 0 || nIndice >= GRALire_Taille())
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRAIndice_Incorrect);
		throw EXCErreur;
	}
	return pGRAtab_Sommets[nIndice];
}


/**********************************************************************************************
Methode GRALire_Tab permettant de v�rifier si un num�ro de sommet est d�j� utilis�
***********************************************************************************************
Entr�e : le num�ro du sommet � v�rifier
N�cessite : n�ant
Sortie : Vrai/faux
Entra�ne : (Retourne un �l�ment de type bool)
**********************************************************************************************/
bool CGraphe::GRACherche_Tab(int eNumSommet)
{
	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eNumSommet)
		{
			return true;
		}
	}
	return false;
}


/***********************************************************************************************
M�thode GRASupprimer_Sommet permettant de supprimer un sommet du graphe
***********************************************************************************************
Entr�e : le num�ro du sommet � supprimer
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception GRASupprimer_Sommet_Impossible : Suppression du sommet impossible)
**********************************************************************************************/
void CGraphe::GRASupprimer_Sommet(int eNumero_Sommet)
{
	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eNumero_Sommet)
		{
			for (unsigned int nBoucle_j = nBoucle_i; nBoucle_j < GRALire_Taille() - 1; nBoucle_j++)
			{
				*GRALire_Tab(nBoucle_j) = *GRALire_Tab(nBoucle_j + 1);
			}
		}
	}

	CSommet** pTabRealloc = (CSommet**)realloc(pGRAtab_Sommets, (GRALire_Taille() - 1) * sizeof(CSommet*));
	if (!pTabRealloc)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRASupprimer_Sommet_Impossible);
		throw EXCErreur;
	}
	pGRAtab_Sommets = pTabRealloc;
	nNbSommets--;
}


/**********************************************************************************************
M�thode GRAAjouter_Sommet permettant d'ajouter un sommet au graphe
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception GRAAjouter_Sommet_Impossible : Ajout du sommet impossible) ou
		   (Exception GRASommet_Deja_Existant : Numero de sommet d�ja existant)
**********************************************************************************************/
void CGraphe::GRAAjouter_Sommet(int eNumero_Sommet)
{
	CSommet** pTabRealloc = (CSommet**)realloc(pGRAtab_Sommets, (GRALire_Taille() + 1) * sizeof(CSommet*));
	if (!pTabRealloc)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRAAjouter_Sommet_Impossible);
		throw EXCErreur;
	}
	pGRAtab_Sommets = pTabRealloc;

	if (GRACherche_Tab(eNumero_Sommet))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRASommet_Deja_Existant);
		throw EXCErreur;
	}
	CSommet* SOMsommet = new CSommet(eNumero_Sommet);
	pGRAtab_Sommets[GRALire_Taille()] = SOMsommet;

	nNbSommets++;
}


/**********************************************************************************************
M�thode GRAModifier_Sommet permettant de modifier un sommet du graphe
***********************************************************************************************
Entr�e : le num�ro du sommet � modifier
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception GRASommet_Inexistant : Sommet donn� n'existe pas) ou
		   (Exception GRASommet_Deja_Pr�sent : Sommet d�ja pr�sent)
**********************************************************************************************/
void CGraphe::GRAModifier_Sommet(int eNumero_Sommet, int eNouveau_Numero)
{
	if (!GRACherche_Tab(eNumero_Sommet))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRASommet_Inexistant);
		throw EXCErreur;
	}

	if (GRACherche_Tab(eNouveau_Numero))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(GRASommet_Deja_Pr�sent);
		throw EXCErreur;
	}

	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eNumero_Sommet)
		{
			GRALire_Tab(nBoucle_i)->SOMModifier_Numero(eNouveau_Numero);
		}
	}
}


/***********************************************************************************************
M�thode GRASupprimer_Arc permettant de supprimer un arc du graphe
***********************************************************************************************
Entr�e : le num�ro des 2 sommets de l'arc � supprimer
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CGraphe::GRASupprimer_Arc(int eSommet_Depart, int eSommet_Destination)
{
	for(unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Depart) // Sommet de d�part trouv�
		{
			for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Partant(); nBoucle_j++)
			{
				if (GRALire_Tab(nBoucle_i)->SOMLire_Partant(nBoucle_j)->ARCLire_Numero_Destination() == eSommet_Destination)
				// L'arc reliant les 2 sommets existe, on le supprime du tableau partant
				{
					GRALire_Tab(nBoucle_i)->SOMSupprimer_Partant(GRALire_Tab(nBoucle_i)->SOMLire_Partant(nBoucle_j));
				}
			}
		}

		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Destination) // Sommet de destination trouv�
		{
			for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Arrivant(); nBoucle_j++)
			{
				if (GRALire_Tab(nBoucle_i)->SOMLire_Arrivant(nBoucle_j)->ARCLire_Numero_Destination() == eSommet_Depart)
				// L'arc reliant les 2 sommets existe, on le supprime du tableau arrivant
				{
					GRALire_Tab(nBoucle_i)->SOMSupprimer_Arrivant(GRALire_Tab(nBoucle_i)->SOMLire_Arrivant(nBoucle_j));
				}
			}
		}
	}
}


/***********************************************************************************************
M�thode GRAModifier_Arc permettant de modifier un arc du graphe
***********************************************************************************************
Entr�e : le num�ro des 2 sommets de l'arc � modifier
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CGraphe::GRAModifier_Arc(int eSommet_Depart, int eSommet_Destination, int eNouvelle_Destination)
{
	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Depart) // Sommet de d�part trouv�
		{
			for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Partant(); nBoucle_j++)
			{
				if (GRALire_Tab(nBoucle_i)->SOMLire_Partant(nBoucle_j)->ARCLire_Numero_Destination() == eSommet_Destination)
				// L'arc reliant les 2 sommets existe, on le modifie dans le tableau partant
				{
					GRALire_Tab(nBoucle_i)->SOMLire_Partant(nBoucle_j)->ARCModifier_Numero_Destination(eNouvelle_Destination);
				}
			}
		}

		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Destination) // Sommet de destination trouv�
		{
			for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Arrivant(); nBoucle_j++)
			{
				if (GRALire_Tab(nBoucle_i)->SOMLire_Arrivant(nBoucle_j)->ARCLire_Numero_Destination() == eSommet_Depart)
				// L'arc reliant les 2 sommets existe, on le supprime du tableau arrivant
				{
					GRALire_Tab(nBoucle_i)->SOMSupprimer_Arrivant(GRALire_Tab(nBoucle_i)->SOMLire_Arrivant(nBoucle_j));
				}
			}
		}

		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eNouvelle_Destination) // Nouveau sommet trouv�
		{
			GRALire_Tab(nBoucle_i)->SOMAjouter_Arrivant(new CArc(eSommet_Depart));
			// On ajoute l'arc dans le tableau arrivant du nouveau sommet
		}
	}
}


/***********************************************************************************************
M�thode GRAAjouter_Arc permettant d'ajouter un arc au graphe
***********************************************************************************************
Entr�e : le num�ro des 2 sommets de l'arc � ajouter
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CGraphe::GRAAjouter_Arc(int eSommet_Depart, int eSommet_Destination)
{
	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Depart) // Sommet de d�part trouv�
		{
			GRALire_Tab(nBoucle_i)->SOMAjouter_Partant(new CArc(eSommet_Destination));
		}

		if (GRALire_Tab(nBoucle_i)->SOMLire_Numero() == eSommet_Destination) // Sommet de destination trouv�
		{
			GRALire_Tab(nBoucle_i)->SOMAjouter_Arrivant(new CArc(eSommet_Depart));
		}
	}
}

/**********************************************************************************************
Constructeur permettant d�initialiser un graphe � partir d'un Parser
***********************************************************************************************
Entr�e : un objet CParser
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
void CGraphe::GRAInitialiser_graphe(CParser PARParam) {

	for (unsigned int nBoucle_i = 0; nBoucle_i < PARParam.PARLire_Nb_Sommets(); nBoucle_i++) {
		GRAAjouter_Sommet(PARParam.PARLire_Tab_Sommets(nBoucle_i));
	}
	
	for (unsigned int nBoucle_j = 0; nBoucle_j < PARParam.PARLire_Nb_Arcs(); nBoucle_j++) {
		GRAAjouter_Arc(PARParam.PARLire_Tab_Depart(nBoucle_j), PARParam.PARLire_Tab_Arrive(nBoucle_j));
	}

}

/**********************************************************************************************
M�thode GRAInverse_Graphe permettant d'inverser tous les arcs d'un graphe
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : un pointeur vers un nouveau graphe inverse de celui appel�
Entra�ne : (Un pointeur vers un graphe est renvoy�)
***********************************************************************************************/
CGraphe* CGraphe::GRAInverse_Graphe()
{
	
	CGraphe *GRAinverse = new CGraphe();
	
	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		GRAinverse->GRAAjouter_Sommet(GRALire_Tab(nBoucle_i)->SOMLire_Numero());
	}

	for (unsigned int nBoucle_i = 0; nBoucle_i < GRALire_Taille(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < GRALire_Tab(nBoucle_i)->SOMLire_Taille_Arrivant(); nBoucle_j++)
		{
			GRAinverse->GRAAjouter_Arc(GRALire_Tab(nBoucle_i)->SOMLire_Numero(), GRALire_Tab(nBoucle_i)->SOMLire_Arrivant(nBoucle_j)->ARCLire_Numero_Destination());
		}
	}
	
	return GRAinverse;
}

