#include <iostream>
#include "CGraphe.h"
#include "CParser.h"



int main(int argc, char *argv[])
{
	if (argc < 2) {
		cout << "Veuillez donner un fichier en argument pour pouvoir creer un graphe ! \n";
		return 1;
	}

	// On d�clare et initialise CParser, on parse le fichier
	CParser PARparser;
	try {
		PARparser = CParser(argv[1]);
	}
	catch (CException EXCErreur) {
		if (EXCErreur.EXCLire_Valeur() == PARFichier_Corrompu) {
			printf("Erreur, fichier corrompu. Ouverture impossible.\n");
			return 1;
		}
		if (EXCErreur.EXCLire_Valeur() == PARFormat_Erreur) {
			printf("Erreur, le fichier n'est pas conforme au format pour creer un graphe .\n");
			return 1;
		}
		if (EXCErreur.EXCLire_Valeur() == PARSommet_Indique_Erreur) {
			printf("Erreur, le nombre de sommets est different au sommets indique sur le fichier .\n");
			return 1;
		}
		if (EXCErreur.EXCLire_Valeur() == PARArc_Indique_Erreur) {
			printf("Erreur, le nombre d'arcs est different au arcs indique sur le fichier .\n");
			return 1;
		}
	}
	// On d�clare un graphe
	CGraphe graphe;

	// A partir du parser, on initialise notre graphe
	try {
		graphe.GRAInitialiser_graphe(PARparser);
	}
	catch (CException EXCErreur) {
		if (EXCErreur.EXCLire_Valeur() == GRAAjouter_Sommet_Impossible) {
			printf("Erreur, ajouter un sommet au graphe impossible.\n");
			return 1;
		}
		if (EXCErreur.EXCLire_Valeur() == GRASommet_Deja_Existant) {
			printf("Erreur, un sommet que l'on veut initialiser existe deja.\n");
			return 1;
		}
		if (EXCErreur.EXCLire_Valeur() == GRAIndice_Incorrect) {
			printf("Erreur, un sommet du graphe n'existe deja.\n");
			return 1;
		}
	}


	// On affiche le graphe
	graphe.GRAAfficher();
	cout << "\n";

	// On inverse le graphe
	cout << "Inverse : \n";
	graphe.GRAInverse_Graphe()->GRAAfficher();


	return 0;
}