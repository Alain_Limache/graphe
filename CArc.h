#pragma once
#ifndef CARC
#define CARC

class CArc
{
private:
	int eARCnumero_Destination;
public:

	/**********************************************************************************************
	Constructeur par d�faut permettant d�initialiser un arc avec sa destination
	***********************************************************************************************
	Entr�e : un entier
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CArc(int eNumero_Destination);

	/**********************************************************************************************
	Constructeur de recopie permettant d�initialiser un arc
	***********************************************************************************************
	Entr�e : un arc
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CArc(CArc & ARCParam);


	/**********************************************************************************************
	M�thode ARCModifier_Numero_Destination permettant de mofifier la valeur de destination de l'arc
	***********************************************************************************************
	Entr�e : un entier
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	inline void ARCModifier_Numero_Destination(int eNumero_Destination);


	/**********************************************************************************************
	M�thode ARCLire_Numero_Destination permettant de lire la valeur de destination de l'arc
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : la valeur de destination
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline int ARCLire_Numero_Destination();
};

#include "CArc.cpp"

#endif
