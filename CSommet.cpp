/**********************************************************************************************
Constructeur par d�faut permettant d�initialiser un sommet
***********************************************************************************************
Entr�e : le num�ro voulu pour le sommet
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
CSommet::CSommet(int eNumero)
{
	pSOMarcs_Partant = (CArc**)malloc(sizeof(CArc*));
	pSOMarcs_Arrivant = (CArc**)malloc(sizeof(CArc*));
	eSOMnumero = eNumero;
	nNbArcsPartant = 0;
	nNbArcsArrivant = 0;
}


/***********************************************************************************************
M�thode SOMLire_Numero permettant de renvoyer le num�ro du sommet
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (Le num�ro du sommet est renvoy�)
**********************************************************************************************/
inline int CSommet::SOMLire_Numero()
{
	return eSOMnumero;
}


/***********************************************************************************************
M�thode SOMModifier_Numero permettant de modifier le num�ro du sommet
***********************************************************************************************
Entr�e : le num�ro voulu pour le sommet
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
inline void CSommet::SOMModifier_Numero(int eNumero)
{
	eSOMnumero = eNumero;
}

/***********************************************************************************************
M�thode SOMLire_Taille_Partant permettant de renvoyer la taille utilis�e du tableau d'arcs partants
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : La taille utilis�e du tableau d'arcs partants
Entra�ne : (La taille utilis�e du tableau d'arcs partants est renvoy�e)
**********************************************************************************************/
unsigned int CSommet::SOMLire_Taille_Partant()
{
	return nNbArcsPartant;
}

/***********************************************************************************************
M�thode SOMLire_Taille_Arrivant permettant de renvoyer la taille utilis�e du tableau d'arcs arrivants
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : La taille utilis�e du tableau d'arcs arrivants
Entra�ne : (La taille utilis�e du tableau d'arcs arrivants est renvoy�e)
**********************************************************************************************/
unsigned int CSommet::SOMLire_Taille_Arrivant()
{
	return nNbArcsArrivant;
}

/***********************************************************************************************
M�thode SOMAjouter_Arrivant permettant d'ajouter un arc arrivant sur ce sommet
***********************************************************************************************
Entr�e : un pointeur sur l'arc � ajouter
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMAjouter_Arrivant_Impossible : Ajout d'un sommet arrivant impossible)
**********************************************************************************************/
void CSommet::SOMAjouter_Arrivant(CArc* pARCParam)
{
	pSOMarcs_Arrivant = (CArc**)realloc(pSOMarcs_Arrivant, (SOMLire_Taille_Arrivant() + 1) * sizeof(CArc*));

	if (!pSOMarcs_Arrivant)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMAjouter_Arrivant_Impossible);
		throw EXCErreur;
	}

	pSOMarcs_Arrivant[SOMLire_Taille_Arrivant()] = pARCParam;
	nNbArcsArrivant++;
}

/***********************************************************************************************
M�thode SOMAjouter_Partant permettant d'ajouter un arc partant de ce sommet
***********************************************************************************************
Entr�e : un pointeur sur l'arc � ajouter
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMAjouter_Partant_Impossible : Ajout d'un sommet partant impossible)
**********************************************************************************************/
void CSommet::SOMAjouter_Partant(CArc* pARCParam)
{
	pSOMarcs_Partant = (CArc**)realloc(pSOMarcs_Partant, (SOMLire_Taille_Partant() + 1) * sizeof(CArc*));

	if (!pSOMarcs_Partant)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMAjouter_Partant_Impossible);
		throw EXCErreur;
	}
	
	pSOMarcs_Partant[SOMLire_Taille_Partant()] = pARCParam;
	nNbArcsPartant++;
}

/***********************************************************************************************
M�thode SOMSupprimer_Arrivant permettant de supprimer un arc arrivant sur ce sommet
***********************************************************************************************
Entr�e : un pointeur sur l'arc � supprimer
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMSupprimer_Arrivant_Impossible : Suppression d'un sommet arrivant 
														 impossible )
**********************************************************************************************/
void CSommet::SOMSupprimer_Arrivant(CArc* pARCParam)
{
	if (!pSOMarcs_Arrivant)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMSupprimer_Arrivant_Impossible);
		throw EXCErreur;
	}

	unsigned int nBoucle_i = 0;
	while (pSOMarcs_Arrivant[nBoucle_i] != NULL)
	{
		if (pSOMarcs_Arrivant[nBoucle_i] == pARCParam)
		{
			for (unsigned int nBoucle_j = nBoucle_i; nBoucle_j < SOMLire_Taille_Arrivant() - 1; nBoucle_j++)
			{
				pSOMarcs_Arrivant[nBoucle_j] = pSOMarcs_Arrivant[nBoucle_j + 1];
			}
		}
		nBoucle_i++;
	}
	pSOMarcs_Arrivant = (CArc**)realloc(pSOMarcs_Arrivant, (SOMLire_Taille_Arrivant() - 1)*sizeof(CArc*));

	nNbArcsArrivant--;
}

/***********************************************************************************************
M�thode SOMSupprimer_Partant permettant de supprimer un arc partant de ce sommet
***********************************************************************************************
Entr�e : un pointeur sur l'arc � supprimer
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMSupprimer_Partant_Impossible : Suppression d'un sommet partant 
														impossible )
**********************************************************************************************/
void CSommet::SOMSupprimer_Partant(CArc* pARCParam)
{
	if (!pSOMarcs_Partant)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMSupprimer_Partant_Impossible);
		throw EXCErreur;
	}

	unsigned int nBoucle_i = 0;
	while (pSOMarcs_Partant[nBoucle_i] != NULL)
	{
		if (pSOMarcs_Partant[nBoucle_i] == pARCParam)
		{
			for (unsigned int nBoucle_j = nBoucle_i; nBoucle_j < SOMLire_Taille_Partant() - 1; nBoucle_j++)
			{
				pSOMarcs_Partant[nBoucle_j] = pSOMarcs_Partant[nBoucle_j + 1];
			}
		}
		nBoucle_i++;
	}
	pSOMarcs_Partant = (CArc**)realloc(pSOMarcs_Partant, (SOMLire_Taille_Partant() - 1) * sizeof(CArc*));
	nNbArcsPartant--;
}

/***********************************************************************************************
M�thode SOMLire_Partant permettant de renvoyer le contenu du tableau d'arcs partants
***********************************************************************************************
Entr�e : l'indice dans le tableau de l'arc � renvoyer
N�cessite : n�ant
Sortie : Un pointeur sur l'arc pSOMarcs_Partant[nIndice]
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMIndice_Incorrect : indice donn�e incorrect)
**********************************************************************************************/
CArc* CSommet::SOMLire_Partant(unsigned int nIndice)
{
	if (nIndice < 0 || nIndice >= SOMLire_Taille_Partant())
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMIndice_Incorrect);
		throw EXCErreur;
	}
	return pSOMarcs_Partant[nIndice];
}

/***********************************************************************************************
M�thode SOMLire_Arrivant permettant de renvoyer le contenu du tableau d'arcs arrivants
***********************************************************************************************
Entr�e : l'indice dans le tableau de l'arc � renvoyer
N�cessite : n�ant
Sortie : Un pointeur sur l'arc pSOMarcs_Arrivant[nIndice]
Entra�ne : (L�objet en cours est modifi�) ou
		   (Exception SOMIndice_Incorrect : indice donn�e incorrect)
**********************************************************************************************/
CArc* CSommet::SOMLire_Arrivant(unsigned int nIndice)
{
	if (nIndice < 0 || nIndice >= SOMLire_Taille_Arrivant())
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(SOMIndice_Incorrect);
		throw EXCErreur;
	}
	return pSOMarcs_Arrivant[nIndice];
}