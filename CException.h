#pragma once
#ifndef CEXCEPTION
#define CEXCEPTION

class CException
{
	private:
		int eEXCValeur;

	public:

		/**********************************************************************************************
		Constructeur par d�faut permettant d�initialiser une exception
		***********************************************************************************************
		Entr�e : rien
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�)
		**********************************************************************************************/
		CException();


		/**********************************************************************************************
		Constructeur de recopie permettant d�initialiser une exception
		***********************************************************************************************
		Entr�e : une exception
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�)
		**********************************************************************************************/
		CException(CException & EXCParam);


		/**********************************************************************************************
		Methode EXCModifier_Valeur permettant de mofifier la valeur de l'exception
		***********************************************************************************************
		Entr�e : un entier
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est modifi�) 
		**********************************************************************************************/
		inline void EXCModifier_Valeur(int eValeur);


		/**********************************************************************************************
		Methode EXCLire_Valeur permettant de lire la valeur de l'exception
		***********************************************************************************************
		Entr�e : rien
		N�cessite : n�ant
		Sortie : la valeur de l'exception
		Entra�ne : (Retourne un entier) 
		**********************************************************************************************/
		inline int EXCLire_Valeur();
};

#include "CException.cpp"

#endif
