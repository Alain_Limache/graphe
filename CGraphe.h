#pragma once
#ifndef CGRAPHE
#define CGRAPHE

#include "CException.h"
#include "CSommet.h"
#include "CParser.h"

#define GRASommet_Deja_Existant 10
#define GRAAjouter_Sommet_Impossible 11
#define GRASupprimer_Sommet_Impossible 12
#define GRAIndice_Incorrect 13
#define GRASommet_Inexistant 14
#define GRASommet_Deja_Pr�sent 15

class CGraphe
{
private:
	CSommet** pGRAtab_Sommets; // Tableau � une dimension qui stocke les sommets du graphe
	unsigned int nNbSommets; // Naturel n�cessaire pour compter le nombre de sommets effectifs

public:

	/**********************************************************************************************
	Constructeur par d�faut permettant d�initialiser un graphe
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CGraphe();


	/**********************************************************************************************
	Constructeur de recopie permettant d�initialiser un graphe
	***********************************************************************************************
	Entr�e : un graphe
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CGraphe(CGraphe& GRAParam);


	/**********************************************************************************************
	M�thode GRAInitialiser_graphe permettant d'initialiser 
	***********************************************************************************************
	Entr�e : un parser
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	void GRAInitialiser_graphe(CParser PARParam);


	/**********************************************************************************************
	M�thode GRAAfficher permettant d'afficher le graphe
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : Affichage console
	Entra�ne : (Affichage console du graphe)
	**********************************************************************************************/
	void GRAAfficher();


	/**********************************************************************************************
	M�thode GRALire_Taille permettant de renvoyer le nombre de sommets du graphe
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : La taille utilis�e du tableau pGRAtab_Sommets
	Entra�ne : (La taille du tableau / le nombre de sommets est renvoy�(e))
	**********************************************************************************************/
	unsigned int GRALire_Taille();


	/**********************************************************************************************
	Methode GRALire_Tab permettant de lire un sommet du graphe
	***********************************************************************************************
	Entr�e : l'indice du sommet dans le tableau du graphe
	N�cessite : n�ant
	Sortie : un pointeur sur un sommet du tableau
	Entra�ne : (Retourne un �l�ment de type CSommet*) ou
			   (Exception GRAIndice_Incorrect : indice donn�e incorrect)
	**********************************************************************************************/
	inline CSommet* GRALire_Tab(unsigned int nIndice);


	/**********************************************************************************************
	Methode GRACherche_Tab permettant de v�rifier si un num�ro de sommet est d�j� utilis�
	***********************************************************************************************
	Entr�e : le num�ro du sommet � v�rifier
	N�cessite : n�ant
	Sortie : Vrai/faux
	Entra�ne : (Retourne un �l�ment de type bool)
	**********************************************************************************************/
	bool GRACherche_Tab(int eNumSommet);


	/**********************************************************************************************
	M�thode GRASupprimer_Sommet permettant de supprimer un sommet du graphe
	***********************************************************************************************
	Entr�e : le num�ro du sommet � supprimer
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception GRASupprimer_Sommet_Impossible : Suppression du sommet impossible)
	**********************************************************************************************/
	void GRASupprimer_Sommet(int eNumero_Sommet);


	/**********************************************************************************************
	M�thode GRAAjouter_Sommet permettant d'ajouter un sommet au graphe
	***********************************************************************************************
	Entr�e : le num�ro du sommet � ajouter
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception GRAAjouter_Sommet_Impossible : Ajout du sommet impossible) ou
		       (Exception GRASommet_Deja_Existant : Num�ro de sommet d�ja existant)
	**********************************************************************************************/
	void GRAAjouter_Sommet(int eNumero_Sommet);


	/**********************************************************************************************
	M�thode GRAModifier_Sommet permettant de modifer un sommet du graphe
	***********************************************************************************************
	Entr�e : le num�ro du sommet � modifier
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�) ou
		       (Exception GRASommet_Inexistant : Sommet donn� n'existe pas) ou
		       (Exception GRASommet_Deja_Pr�sent : Sommet d�ja pr�sent)
	**********************************************************************************************/
	void GRAModifier_Sommet(int eNumero_Sommet, int eNouveau_Numero);


	/**********************************************************************************************
	M�thode GRASupprimer_Arc permettant de supprimer un arc du graphe
	***********************************************************************************************
	Entr�e : le num�ro des 2 sommets de l'arc � supprimer
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	void GRASupprimer_Arc(int eSommet_Depart, int eSommet_Destination);


	/**********************************************************************************************
	M�thode GRAModifier_Arc permettant de modifier un arc du graphe
	***********************************************************************************************
	Entr�e : le num�ro des 2 sommets de l'arc � modifier
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	void GRAModifier_Arc(int eSommet_Depart, int eSommet_Destination, int eNouvelle_Destination);


	/**********************************************************************************************
	M�thode GRAAjouter_Arc permettant d'ajouter un arc au graphe
	***********************************************************************************************
	Entr�e : le num�ro des 2 sommets de l'arc � ajouter
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	void GRAAjouter_Arc(int eSommet_Depart, int eSommet_Destination);

	/**********************************************************************************************
	M�thode GRAInverse_Graphe permettant d'inverser tous les arcs d'un graphe
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : un pointeur vers un nouveau graphe inverse de celui appel�
	Entra�ne : (Un pointeur vers un graphe est renvoy�)
	***********************************************************************************************/
	CGraphe* GRAInverse_Graphe();

};

#include "CGraphe.cpp"

#endif