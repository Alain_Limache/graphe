#include <iostream>
#include <fstream>
#include <string>


/**********************************************************************************************
Constructeur par d�faut permettant d�initialiser un parser
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
CParser::CParser() {

}

/**********************************************************************************************
Constructeur permettant d�initialiser un parser � partir d'un fichier
***********************************************************************************************
Entr�e : le nom d'un fichier
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�) ou 
		   (Exception PARFichier_Corrompu : le fichier est corrompu) ou
		   (Exception PARFormat_Erreur : le format du fichier ne correspond pas) ou 
		   (Exception PARSommet_Indique_Erreur : le nombre de sommets indiqu� 
												 n'est pas �gale au nombre sommet donn�e) ou
		   (Exception PARArc_Indique_Erreur : le nombre d'arcs indiqu�
											  n'est pas �gale au nombre sommet donn�e) 
**********************************************************************************************/
CParser::CParser(char* sNom_Fichier) {

	char* sLigneTxt = new char[1024]; // Cha�ne de caract�res qui va contenir les lignes du fichier 1 par 1
	char* sParcoursLigne; // 2�me cha�ne de caract�res qui va servir � d�couper sLigneTxt pour r�cup�rer les infos importantes
	ifstream monFichier(sNom_Fichier);
	
	if (!monFichier.is_open())
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(PARFichier_Corrompu);
		throw EXCErreur;
	}
	else
	{
		unsigned int nBoucle_ligneTxt = 0; // Variable pour savoir � quelle ligne on se situe dans le fichier
		int eLigne_Fin_Sommets = 0; // Variable de ligne de la fin de la partie Numero de Sommet
		int eSommet = 0; // Sommet en cours
		int eArc = 0; // Arc en cours
		
		while (monFichier.getline(sLigneTxt, 1024))
		{
			if (nBoucle_ligneTxt == 0) {
				sParcoursLigne = strstr(sLigneTxt, "NBSommets=");
				nNbSommets = stoi(sParcoursLigne + 10);
				eLigne_Fin_Sommets = PARLire_Nb_Sommets()+4;

			} else if (nBoucle_ligneTxt == 1) {
				sParcoursLigne = strstr(sLigneTxt, "NBArcs=");
				nNbArcs = stoi(sParcoursLigne + 7);

			} else if (nBoucle_ligneTxt == 2) {
				peTableau_Numero_Sommets = (int*)malloc(PARLire_Nb_Sommets() * sizeof(int));
				/*On profite de la ligne pour allouer le tableau de sommets avec les variables pr�c�demment initialis�es
				D'o� l'importance de bien respecter la nomenclature du fichier*/

			} else if (nBoucle_ligneTxt == eLigne_Fin_Sommets) {
				peTableau_Arc_Depart = (int*)malloc(PARLire_Nb_Arcs() * sizeof(int));
				peTableau_Arc_Arrive = (int*)malloc(PARLire_Nb_Arcs() * sizeof(int));
				/*On profite de la ligne pour allouer les tableaux des arcs avec les variables pr�c�demment initialis�es
				D'o� l'importance de bien respecter la nomenclature du fichier*/

			} else {
				if (sLigneTxt[0] != ']') // Quand cette ligne est atteinte, il ne faut plus essayer de remplir 
				{
					if (sLigneTxt[0] == 'N') {
						sParcoursLigne = strstr(sLigneTxt, "Numero=");
						peTableau_Numero_Sommets[eSommet] = stoi(sParcoursLigne + 7);
						eSommet++;
					} else if(sLigneTxt[0] == 'D') {
						sParcoursLigne = strstr(sLigneTxt, "Debut=");
						peTableau_Arc_Depart[eArc] = stoi(sParcoursLigne + 6);
						
						sParcoursLigne = strstr(sLigneTxt, "Fin=");
						peTableau_Arc_Arrive[eArc] = stoi(sParcoursLigne + 4);
						eArc++;
					}
					else {
						CException EXCErreur;
						EXCErreur.EXCModifier_Valeur(PARFormat_Erreur);
						throw EXCErreur;
					}
				}
			}
			nBoucle_ligneTxt++;
		}

		if (eSommet != nNbSommets) {
			CException EXCErreur;
			EXCErreur.EXCModifier_Valeur(PARSommet_Indique_Erreur);
			throw EXCErreur;
		}

		if (eArc != nNbArcs) {
			CException EXCErreur;
			EXCErreur.EXCModifier_Valeur(PARArc_Indique_Erreur);
			throw EXCErreur;
		}
	}
}

/**********************************************************************************************
M�thode PARModifier_Nb_Sommets permettant de modifier le nombre de sommet
***********************************************************************************************
Entr�e : le nombre de sommets
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CParser::PARModifier_Nb_Sommets(unsigned int nParam_Nb_Sommets) {
	nNbSommets = nParam_Nb_Sommets;
}

/**********************************************************************************************
M�thode PARModifier_Nb_Arcs permettant de modifier le nombre d'arcs
***********************************************************************************************
Entr�e : le nombre d'arcs
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CParser::PARModifier_Nb_Arcs(unsigned int nParam_Nb_Arcs) {
	nNbArcs = nParam_Nb_Arcs;
}

/**********************************************************************************************
M�thode PARLire_Nb_Sommets permettant de lire le nombre de sommet
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (Retourne un entier)
**********************************************************************************************/
unsigned int CParser::PARLire_Nb_Sommets() {
	return nNbSommets;
}

/**********************************************************************************************
M�thode PARLire_Nb_Arcs permettant de lire le nombre d'arcs
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (Retourne un entier)
**********************************************************************************************/
unsigned int CParser::PARLire_Nb_Arcs() {
	return nNbArcs;
}

/**********************************************************************************************
M�thode PARLire_Tab_Sommets permettant de lire l'entier du tableau sommets � la position donn�e
***********************************************************************************************
Entr�e : la position
N�cessite : n�ant
Sortie : l'�l�ment du tableau sommet � la position donn�e
Entra�ne : (Retourne un entier)
**********************************************************************************************/
int CParser::PARLire_Tab_Sommets(unsigned int nPosition) {
	return peTableau_Numero_Sommets[nPosition];
}

/**********************************************************************************************
M�thode PARLire_Tab_Depart permettant de lire l'entier du tableau arcs des d�parts �
la position donn�e
***********************************************************************************************
Entr�e : la position
N�cessite : n�ant
Sortie : l'�l�ment du tableau arc des departs � la position donn�e
Entra�ne : (Retourne un entier)
**********************************************************************************************/
int CParser::PARLire_Tab_Depart(unsigned int nPosition) {
	return peTableau_Arc_Depart[nPosition];
}

/**********************************************************************************************
M�thode PARLire_Tab_Arrive permettant de lire l'entier du tableau arcs des arriv�s �
la position donn�e
***********************************************************************************************
Entr�e : la position
N�cessite : n�ant
Sortie : l'�l�ment du tableau arc des arriv�s � la position donn�e
Entra�ne : (Retourne un entier)
**********************************************************************************************/
int CParser::PARLire_Tab_Arrive(unsigned int nPosition) {
	return peTableau_Arc_Arrive[nPosition];
}