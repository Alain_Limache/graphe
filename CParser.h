#pragma once
#ifndef CPARSER
#define CPARSER

using namespace std;

#define PARFichier_Corrompu 30
#define PARFormat_Erreur 31
#define PARSommet_Indique_Erreur 32
#define PARArc_Indique_Erreur 33

class CParser
{
private:
	int nNbSommets; 
	int nNbArcs; 
	int * peTableau_Numero_Sommets;
	int * peTableau_Arc_Depart;
	int * peTableau_Arc_Arrive;

public:

	/**********************************************************************************************
	Constructeur par d�faut permettant d�initialiser un parser
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�)
	**********************************************************************************************/
	CParser();

	/**********************************************************************************************
	Constructeur permettant d�initialiser un parser � partir d'un fichier
	***********************************************************************************************
	Entr�e : le nom d'un fichier
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est initialis�) ou
			   (Exception PARFichier_Corrompu : le fichier est corrompu) ou
			   (Exception PARFormat_Erreur : le format du fichier ne correspond pas) ou
			   (Exception PARSommet_Indique_Erreur : le nombre de sommets indiqu�
													 n'est pas �gale au nombre sommet donn�e) ou
			   (Exception PARArc_Indique_Erreur : le nombre d'arcs indiqu�
												  n'est pas �gale au nombre sommet donn�e)
	**********************************************************************************************/
	CParser(char* sNom_Fichier);

	/**********************************************************************************************
	M�thode PARModifier_Nb_Sommets permettant de modifier le nombre de sommet
	***********************************************************************************************
	Entr�e : le nombre de sommets
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	inline void PARModifier_Nb_Sommets(unsigned int nParam_Nb_Sommets);

	/**********************************************************************************************
	M�thode PARModifier_Nb_Arcs permettant de modifier le nombre d'arcs
	***********************************************************************************************
	Entr�e : le nombre d'arcs
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (L�objet en cours est modifi�)
	**********************************************************************************************/
	inline void PARModifier_Nb_Arcs(unsigned int nParam_Nb_Arcs);

	/**********************************************************************************************
	M�thode PARLire_Nb_Sommets permettant de lire le nombre de sommet
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline unsigned int  PARLire_Nb_Sommets();

	/**********************************************************************************************
	M�thode PARLire_Nb_Arcs permettant de lire le nombre d'arcs
	***********************************************************************************************
	Entr�e : rien
	N�cessite : n�ant
	Sortie : rien
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline unsigned int PARLire_Nb_Arcs();



	/**********************************************************************************************
	M�thode PARLire_Tab_Sommets permettant de lire l'entier du tableau sommets � la position donn�e
	***********************************************************************************************
	Entr�e : la position
	N�cessite : n�ant
	Sortie : l'�l�ment du tableau sommet � la position donn�e
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline int PARLire_Tab_Sommets(unsigned int nPosition);


	/**********************************************************************************************
	M�thode PARLire_Tab_Depart permettant de lire l'entier du tableau arcs des d�parts � 
	la position donn�e
	***********************************************************************************************
	Entr�e : la position
	N�cessite : n�ant
	Sortie : l'�l�ment du tableau arc des departs � la position donn�e
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline int PARLire_Tab_Depart(unsigned int nPosition);


	/**********************************************************************************************
	M�thode PARLire_Tab_Arrive permettant de lire l'entier du tableau arcs des arriv�s �
	la position donn�e
	***********************************************************************************************
	Entr�e : la position
	N�cessite : n�ant
	Sortie : l'�l�ment du tableau arc des arriv�s � la position donn�e
	Entra�ne : (Retourne un entier)
	**********************************************************************************************/
	inline int PARLire_Tab_Arrive(unsigned int nPosition);

};

#include "CParser.cpp"

#endif