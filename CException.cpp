
/**********************************************************************************************
Constructeur par d�faut permettant d�initialiser une exception
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
CException::CException()
{
	eEXCValeur = 0;
}


/**********************************************************************************************
Constructeur de recopie permettant d�initialiser une exception
***********************************************************************************************
Entr�e : une exception
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est initialis�)
**********************************************************************************************/
CException::CException(CException & EXCParam)
{
	eEXCValeur = EXCParam.eEXCValeur;
}


/**********************************************************************************************
Methode EXCModifier_Valeur permettant de mofifier la valeur de l'exception
***********************************************************************************************
Entr�e : un entier
N�cessite : n�ant
Sortie : rien
Entra�ne : (L�objet en cours est modifi�)
**********************************************************************************************/
void CException::EXCModifier_Valeur(int eValeur)
{
	eEXCValeur = eValeur;
}


/**********************************************************************************************
Methode EXCLire_Valeur permettant de lire la valeur de l'exception
***********************************************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : la valeur de l'exception
Entra�ne : (Retourne un entier)
**********************************************************************************************/
int CException::EXCLire_Valeur()
{
	return eEXCValeur;
}


